/* eslint-disable no-unused-vars */
const maxRunTime = 9;
const matrixSize = 3;
let nextPlayer = 'X';
let numRunTimes = 1;
let gameResult = null;
const resultSet = {};
resultSet.X = [];
resultSet.O = [];

function layDownSymbol (elem) {
  if (!elem.innerText && !gameResult) {
    elem.innerText = nextPlayer;
    const elemId = [].indexOf.call(elem.parentNode.children, elem);
    resultSet[nextPlayer].push(elemId + 1);
    resultSet[nextPlayer].sort((a, b) => a - b);
    determineWhoWins(resultSet[nextPlayer], nextPlayer);

    // update the next player
    numRunTimes++;
    nextPlayer = nextPlayer === 'X' ? 'O' : 'X';
    const whosNext = document.getElementById('next-round');
    whosNext.innerText = 'Next Round: ' + nextPlayer;
  }
}

function determineWhoWins (stepsArray, player) {
  const gameStatus = document.getElementById('game-status');
  if (numRunTimes < 5) {
    // both parties have less than 3 steps, thus no result yet
    gameStatus.innerText = 'Playing ^_^.';
  } else {
    let i = 0;
    while (i < stepsArray.length && !gameResult) {
      // iterate over the records
      for (let j = i + 1; j < stepsArray.length; j++) {
        const lastNumber = stepsArray[j] * 2 - stepsArray[i];
        /* if statement to make sure that the first number is on the first row/column
        and the last number is in the record and on the last row/column */
        if (stepsArray.includes(lastNumber) &&
             (Math.ceil(lastNumber / matrixSize) === matrixSize || lastNumber % matrixSize === 0) &&
             (Math.floor(stepsArray[i] / matrixSize) === 0 || stepsArray[i] % matrixSize === 1)) {
          gameStatus.innerText = 'Congratulations! ' + player + ' wins.';
          gameResult = 'over';
          break;
        } else {
          if (numRunTimes === maxRunTime) {
            gameStatus.innerText = 'The game ends as a tie.';
          } else {
            gameStatus.innerText = 'Playing ^_^.';
          }
        }
      }
      i++;
    }
  }
}

function resetGame () {
  const elems = document.querySelectorAll('div[class^="grid-box"]');
  [].forEach.call(elems, function (elem) {
    elem.innerText = '';
  });

  //   reset all the parameters
  numRunTimes = 1;
  resultSet.X = [];
  resultSet.O = [];
  nextPlayer = 'X';
  gameResult = null;
  determineWhoWins();
  const whosNext = document.getElementById('next-round');
  whosNext.innerText = 'Next Round: ' + nextPlayer;
}

window.onload = function () {
  this.resetGame();
};
